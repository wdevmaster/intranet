<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Closebox extends Admin_Controller {

    function __construct() {
		parent::__construct();
		$this->load->model("expense_m");
		$this->load->model('invoice_m');

        $language = $this->session->userdata('lang');
		$this->lang->load('closebox', $language);
		$this->load->library('excel');
    }
    
    public function index() {
        $this->data['headerassets'] = array(
			'css' => array(
				'assets/daterangepicker/daterangepicker.css',
			),
			'js' => array(
				'assets/daterangepicker/moment.min.js', 
                'assets/daterangepicker/daterangepicker.js',
			)
		);

		$this->data["subview"] = "closebox/index";
		$this->load->view('_layout_main', $this->data);
	}

	public function data_get()
	{
		if  (!$this->input->get('start') && !$this->input->get('end'))
			return $this->response(array('error' => 'Entity could not be created'), 404);
		else {
			$start 	= $this->input->get('start');
			$end 	= $this->input->get('end');
		}

		header("Content-Type: application/json; charset=UTF-8");
		echo json_encode($this->getDataRange($start, $end));
	}

	public function getDataRange($start, $end)
	{
		$expenseRows =	$this->expense_m->get_expense_rang($start, $end);
		$invoiceRows = $this->invoice_m->get_invoice_rang($start, $end);
		$sutotal = $expenses = 0;

		foreach ($invoiceRows as $invoice)
			$sutotal += ($invoice->amount - $invoice->discount);
		
		foreach ($expenseRows as $expense)
			$expenses += $expense->amount;

		return [
			'invoices' => $invoiceRows,
			'expenses' => $expenseRows,
			'subtotal' => $sutotal,
			'expense' => $expenses,
			'total' => $sutotal - $expenses
		];
	}


	public function excel()
	{
		if  (!$this->input->get('start') && !$this->input->get('end'))
			redirect(base_url("closebox/index"));
		else {
			$start 	= $this->input->get('start');
			$end 	= $this->input->get('end');
		}
		$data = $this->getDataRange($start, $end);
		if ($start != $end )
			$dateExcel = "_{$start}_{$end}";
		else
			$dateExcel = "_{$start}";

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle("Corte Colegio");
		$bordeStyle = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		);
		$index = 1;
	
		$columns = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M' ];
		foreach ($columns as $i => $column) {
			if ($i == 0)
				$this->excel->getActiveSheet()->getColumnDimension($column)->setWidth(8);
			elseif ($i == count($columns)-1)
				$this->excel->getActiveSheet()->getColumnDimension($column)->setWidth(16);
			else
				$this->excel->getActiveSheet()->getColumnDimension($column)->setWidth(10);
			
			$this->excel->getActiveSheet()->getStyle($column.$index)->getFont()->setBold(true);
		}
		
		$this->excel->getActiveSheet()->getStyle("M:M")
					->getNumberFormat()
					->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		$this->excel->getActiveSheet()->getStyle("M:M")
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$this->excel->getActiveSheet()->getStyle('A1:M1')
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		$this->excel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($bordeStyle);
		$this->excel->getActiveSheet()->setCellValue("A{$index}", $this->lang->line('invoice'));
		$this->excel->getActiveSheet()->mergeCells("B{$index}:E{$index}");
		$this->excel->getActiveSheet()->setCellValue("B{$index}", $this->lang->line('name'));
		$this->excel->getActiveSheet()->mergeCells("F{$index}:L{$index}");
		$this->excel->getActiveSheet()->setCellValue("F{$index}", $this->lang->line('concept'));
		$this->excel->getActiveSheet()->setCellValue("M{$index}", $this->lang->line('amount'));
		
		if  (count($data['expenses']) == 0){
			$index++;
			$this->excel->getActiveSheet()->getStyle("A{$index}:M{$index}")->applyFromArray($bordeStyle);
			$this->excel->getActiveSheet()->mergeCells("B{$index}:E{$index}");
			$this->excel->getActiveSheet()->mergeCells("F{$index}:L{$index}");
			$this->excel->getActiveSheet()->getStyle("A{$index}")
						->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle("F{$index}:L{$index}")
						->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$this->excel->getActiveSheet()->setCellValue("A{$index}", "");
			$this->excel->getActiveSheet()->setCellValue("B{$index}", "");
			$this->excel->getActiveSheet()->setCellValue("F{$index}", "");
			$this->excel->getActiveSheet()->setCellValue("M{$index}", "");
		} else
			foreach ($data['invoices'] as $invoice) {
				$index++;

				$this->excel->getActiveSheet()->getStyle("A{$index}:M{$index}")->applyFromArray($bordeStyle);
				$this->excel->getActiveSheet()->mergeCells("B{$index}:E{$index}");
				$this->excel->getActiveSheet()->mergeCells("F{$index}:L{$index}");
				$this->excel->getActiveSheet()->getStyle("A{$index}")
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$this->excel->getActiveSheet()->getStyle("F{$index}:L{$index}")
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->setCellValue("A{$index}", $invoice->invoiceID);
				$this->excel->getActiveSheet()->setCellValue("B{$index}", strtoupper($invoice->stname));
				$this->excel->getActiveSheet()->setCellValue("F{$index}", strtoupper($invoice->feetype));
				$amount = ($invoice->amount + $invoice->discount);
				$this->excel->getActiveSheet()->setCellValue("M{$index}", $amount);
			}

		$footIndex = $index+=1;
		$index+=1;
		$this->excel->getActiveSheet()->getStyle("A{$index}:E{$index}")->applyFromArray($bordeStyle);
		$this->excel->getActiveSheet()->mergeCells("A{$index}:C{$index}");
		$this->excel->getActiveSheet()->getStyle("A{$index}")->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle("A{$index}")
						->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->setCellValue("A{$index}", $this->lang->line('expenses'));
		$this->excel->getActiveSheet()->mergeCells("D{$index}:E{$index}");
		$this->excel->getActiveSheet()->getStyle("D{$index}")->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle("D{$index}")
						->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->setCellValue("D{$index}", $this->lang->line('amount'));

		if  (count($data['expenses']) == 0){
			$index+=1;
			$this->excel->getActiveSheet()->getStyle("A{$index}:E{$index}")->applyFromArray($bordeStyle);
			$this->excel->getActiveSheet()->mergeCells("A{$index}:C{$index}");
			$this->excel->getActiveSheet()->setCellValue("A{$index}", "");
			$this->excel->getActiveSheet()->mergeCells("D{$index}:E{$index}");
			$this->excel->getActiveSheet()->setCellValue("D{$index}", "");
		}else
			foreach ($data['expenses'] as $expense) {
				$index++;
				$this->excel->getActiveSheet()->getStyle("A{$index}:E{$index}")->applyFromArray($bordeStyle);
				$this->excel->getActiveSheet()->mergeCells("A{$index}:C{$index}");
				$this->excel->getActiveSheet()->setCellValue("A{$index}", $expense->expense);
				$this->excel->getActiveSheet()->mergeCells("D{$index}:E{$index}");
				$this->excel->getActiveSheet()->getStyle("D{$index}")
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle("D{$index}")
					->getNumberFormat()
					->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				$this->excel->getActiveSheet()->setCellValue("D{$index}", $expense->amount);
			}
		
		$this->excel->getActiveSheet()->mergeCells("K{$footIndex}:L{$footIndex}");
		$this->excel->getActiveSheet()->setCellValue("K{$footIndex}", $this->lang->line('subtotal'));
		$this->excel->getActiveSheet()->getStyle("M{$footIndex}")->applyFromArray($bordeStyle);
		$nInvoices = count($data['invoices'])+1;
		$this->excel->getActiveSheet()->setCellValue("M{$footIndex}", "=SUM(M2:M{$nInvoices})");
		
		$footIndex++;
		$this->excel->getActiveSheet()->mergeCells("K{$footIndex}:L{$footIndex}");
		$this->excel->getActiveSheet()->setCellValue("K{$footIndex}", $this->lang->line('expenses'));
		$this->excel->getActiveSheet()->getStyle("M{$footIndex}")->applyFromArray($bordeStyle);
		$nExpenses = $footIndex+count($data['expenses'])+1;
		$initNExpenses = count($data['invoices'])+3;
		$this->excel->getActiveSheet()->setCellValue("M{$footIndex}", "=SUM(D{$initNExpenses}:E{$nExpenses})");
		
		$footIndex++;
		$iSub = $footIndex - 2;
		$iExp = $footIndex - 1; 
		$this->excel->getActiveSheet()->mergeCells("K{$footIndex}:L{$footIndex}");
		$this->excel->getActiveSheet()->setCellValue("K{$footIndex}", $this->lang->line('total'));
		$this->excel->getActiveSheet()->getStyle("M{$footIndex}")->applyFromArray($bordeStyle);
		$this->excel->getActiveSheet()->setCellValue("M{$footIndex}", "=M{$iSub}-M{$iExp}");

		$archivo = "corte_colegio_{$dateExcel}.xls";
		if (true) {
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="'.$archivo.'"');
        	header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->setPreCalculateFormulas(true);
			$objWriter->save('php://output');
		}
	}

}