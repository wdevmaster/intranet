
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-sitemap"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_closebox')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <span class="page-header">

                        <div class="col-md-4">
                            <b><?=$this->lang->line('dates')?>:</b>
                            <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>

                        </div>
                    </span>
                </div>

                <hr>

                <div class="row">

                    <div class="col-sm-12">
                        <a id="export-excel" class="btn btn-default export" href="#" target="_blank">Excel</a>
                        <table class="table table-bordered" style="margin-top: 5px"> 
                            <thead>
                                <tr>
                                    <th style="width: 20px;">
                                        <?=$this->lang->line('invoice')?>
                                    </th>
                                    <th ><?=$this->lang->line('name')?></th>
                                    <th style="width: 48%"><?=$this->lang->line('concept')?></th>
                                    <th style="width: 120px;">
                                        <?=$this->lang->line('amount')?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="tbody tbody-invoices">
                                <tr>
                                    <td>000</td>
                                    <td>HECTOR SANTIAGO GARCIA CASTILLO</td>
                                    <td>COELGIATURA SEPTIEMBRE ($2300) 1 MOÑO GRIS ($80)</td>
                                    <td>
                                        <i class="pull-left">$</i>0.000,00
                                    </td>
                                </tr>
                                <tr>
                                    <td>000</td>
                                    <td>MARIA FERNANDA GARZA CALVO</td>
                                    <td>COLEGIATURA AGOSTO C/R (BUENOS $400 DE DIAS DE TRABAJO EN COCINA)</td>
                                    <td>
                                        <i class="pull-left">$</i>0.000,00
                                    </td>
                                </tr>
                                <tr>
                                    <td>000</td>
                                    <td>OSVALDO ETIENNE MORENO GARCIA</td>
                                    <td>INSC. ($2000) MAT. ($1900)LIBROS ($4105)UNIF. ($495)</td>
                                    <td>
                                        <i class="pull-left">$</i>0.000,00
                                    </td>
                                </tr>
                                <tr>
                                    <td>000</td>
                                    <td>HIROMI AZLETH AGUILERA</td>
                                    <td>1 P. #6 Y 1 F. #6</td>
                                    <td>
                                        <i class="pull-left">$</i>0.000,00
                                    </td>
                                </tr>
                                <tr>
                                    <td>000</td>
                                    <td>DIEGO MONTEMAYOR</td>
                                    <td>COLEGIATURA SEPTIEMBRE</td>
                                    <td>
                                        <i class="pull-left">$</i>0.000,00
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2" style="padding: 0;">
                                        <table class="table table-tfoot">
                                            <tr>
                                                <td><?=$this->lang->line('subtotal')?></td>
                                                <td style="width: 120px;">
                                                    <i class="pull-left"><?=$this->data['siteinfos']->currency_symbol?></i>
                                                    <span id="subtotal">0.000,00</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><?=$this->lang->line('expenses')?></td>
                                                <td style="width: 120px;">
                                                    <i class="pull-left"><?=$this->data['siteinfos']->currency_symbol?></i>
                                                    <span id="expense">0.000,00</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><?=$this->lang->line('total')?></td>
                                                <td style="width: 120px;">
                                                    <i class="pull-left"><?=$this->data['siteinfos']->currency_symbol?></i>
                                                    <span id="total">0.000,00</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>

                <div class="row" style="margin-top: 15px">

                    <div class="col-sm-5">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><?=$this->lang->line('expenses')?></th>
                                    <th style="width: 120px;"><?=$this->lang->line('amount')?></th>
                                </tr>
                            </thead>
                            <tbody class="tbody tbody-expenses">
                                <tr>
                                    <td>Gastos 1</td>
                                    <td><i class="pull-left">$</i>0.000,00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--
                    <div class="col-sm-7 text-right">
                        <button class="btn btn-success">Export Excel</button>
                        <button class="btn btn-default">Export CSV</button>
                        <button class="btn btn-danger">Export PDF</button>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>


<style>
#custom_date {
    display: none;
}
.page-header .pull-right,
.page-header .pull-left  {
    display: flex;
}
.control-label {
    color: #555;
    font-size: 14px;
    font-weight: 600;
    padding-top: 4px;
}

.table thead {
    font-weight: 800;
}

.table tfoot {
    font-size: 13px;
}

.table tr td:last-child {
    text-align: right;
    vertical-align: bottom;
    font-weight: 800;
}

.table tr:first-child td:first-child {
    text-align: center;
}

.table-tfoot td:first-child {
    text-align: right!important;
}

.table-tfoot {
    font-weight: 100;
}

.table-tfoot>tbody>tr {
    font-size: 14px;
}

.table-tfoot>tbody>tr:last-child {
    font-size: 16px;
}

.table-tfoot>tbody>tr>td{
    border-right: 1px solid #ddd;
}

.table-tfoot>tbody>tr>td:last-child {
    border-right: none;
}

</style>

<script type="text/javascript">
    var currencySym = "<?=$this->data['siteinfos']->currency_symbol?>"
    var start = moment().startOf('week')
    var end = moment().endOf('week')
    var urlExcel = "<?=base_url("closebox/excel")?>"

    getData(start, end)

    function cb(start, end) {
        var str = ''

        var url = urlExcel +'?start='+start.format('YYYY-MM-DD')+'&'+end.format('YYYY-MM-DD')
        
        if (start.format('DD-MM-YYYY') === end.format('DD-MM-YYYY'))
            str = start.format('DD-MM-YYYY')
        else if (start.format('DD') !== end.format('DD') && start.format('MM-YYYY') === end.format('MM-YYYY'))
            str = start.format('DD') +'/'+end.format('DD-MM-YYYY')
        else if (start.format('DD-MM') !== end.format('DD-MM') && start.format('YYYY') === end.format('YYYY'))
            str = start.format('DD-MM') +'/'+end.format('DD-MM-YYYY')
        else
            str = start.format('DD-MM-YYYY') +'/'+end.format('DD-MM-YYYY')

        $('#export-excel').attr('href', url)
        $('#reportrange span').html(str);
    }

    function getData(start, end) {
        var dat = {
            start: start.format('YYYY-MM-DD'),
            end: end.format('YYYY-MM-DD'),
        }

        $.ajax({
            type: 'GET',
            url: "<?=base_url('closebox/data_get')?>",
            beforeSend: function( xhr ) {
                $('.tbody tr').remove()
                $('.tbody-invoices').append('<tr>'+
                    "<td colspan='4'><?=$this->lang->line('search_data')?></td>"+
                '</tr>')

                $('.tbody-expenses').append('<tr>'+
                    "<td colspan='2'><?=$this->lang->line('search_data')?></td>"+
                '</tr>') 

                $('span#subtotal').text('--')
                $('span#expense').text('--')
                $('span#total').text('--')
            },
            dataType: 'json',
            data: dat,
            success: function(data) {
                $('.tbody tr').remove()

                if (data.invoices.length == 0)
                    $('.tbody-invoices').append('<tr>'+
                        "<td colspan='4'><?=$this->lang->line('no_data')?></td>"+
                    '</tr>')
                else
                    $.each(data.invoices, function(key, value) {
                        $('.tbody-invoices').append('<tr>'+
                            "<td>"+value.invoiceID+"</td>"+
                            "<td>"+value.stname+"</td>"+
                            "<td>"+value.feetype+"</td>"+
                            "<td>"+
                                "<i class='pull-left'>$</i>"+number_format(value.amount - value.discount, 2, ',', '.')+
                            "</td>"+
                        '</tr>')
                    })
                
                if (data.expenses.length == 0)
                    $('.tbody-expenses').append('<tr>'+
                        "<td colspan='2'><?=$this->lang->line('no_data')?></td>"+
                    '</tr>')
                else
                    $.each(data.expenses, function(key, value) {
                        $('.tbody-expenses').append('<tr>'+
                            "<td>"+value.expense+"</td>"+
                            "<td>"+
                                "<i class='pull-left'>$</i>"+number_format(value.amount, 2, ',', '.')+
                            "</td>"+
                        '</tr>')
                    })
        
                $('span#subtotal').text(number_format(data.subtotal, 2, ',', '.'))
                $('span#expense').text(number_format(data.expense, 2, ',', '.'))
                $('span#total').text(number_format(data.total, 2, ',', '.'))
            }
        });
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        locale: {
            "format": "DD-MM-YYYY",
            "customRangeLabel": "<?=$this->lang->line('custom_range')?>",
        },
        ranges: {
           "<?=$this->lang->line('today')?>": [moment(), moment()],
           "<?=$this->lang->line('yesterday')?>": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           "<?=$this->lang->line('last_7_days')?>": [moment().subtract(6, 'days'), moment()],
           "<?=$this->lang->line('last_30_days')?>": [moment().subtract(29, 'days'), moment()],
    
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        getData(picker.startDate, picker.endDate)
    })

    cb(start, end);

    function number_format(number, decimals = 2, dec_point = '.', thousands_sep = ',') {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
</script>
