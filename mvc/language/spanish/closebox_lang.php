<?php

$lang['panel_title']    = "Cierre de caja";

$lang['invoice']        = "Folio";
$lang['name']           = "Nombre";
$lang['concept']        = "Concepto";
$lang['amount']         = "Importe";
$lang['subtotal']       = "Subtotal";
$lang['expenses']       = "Gastos";
$lang['total']          = "Total";

$lang['dates']          = "Fecha";

$lang['today']          = "Hoy";
$lang['yesterday']      = "Ayer";
$lang['last_7_days']    = "Ultimos 7 días";
$lang['last_30_days']   = "Ultimos 30 días";
$lang['this_month']     = "Este mes";
$lang['custom_range']   = "Rango personalizado";

$lang['search_data']    = "Buscando registros...";
$lang['no_data']        = "Sin registros";